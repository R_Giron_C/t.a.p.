#include <stdio.h>
#include <stdlib.h>
#include <iostream>

using namespace std;

int main()
{
	int num;
	cout<<"Ingrese la cantidad de numeros de la serie: ";
	cin>>num;
	
	long long int fibo[100];
	fibo[0]=0;
	fibo[1]=1;
	
	cout<<"Serie Fibonacci:"<<endl;
	cout<<fibo[0]<<endl<<fibo[1]<<endl;
	
	for(int i=2;i<num;i++)
	{
		fibo[i]=fibo[i-1]+fibo[i-2];
		cout<<fibo[i]<<endl;
	}
	return 0;
}
