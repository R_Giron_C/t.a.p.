#include <stdio.h>
#include <stdlib.h>
#include <iostream>

using namespace std;

int main()
{
	int fact=1, num;
	cout<<"Ingresa un numero: ";
	cin>>num;
	cout<<"\nEl factorial de "<<num<<" es: \n"<<endl;
	for(int i=1;i<num+1;i++)
	{
		fact=fact*i;
		cout<<i<<" * ";
	}
	
	cout<<" = "<<fact;
	return 0;
}
