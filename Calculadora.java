import javax.swing.*;
import java.awt.event.*;
import java.awt.FlowLayout;

public class Calculadora extends JFrame implements ActionListener
{
	JButton boton1=new JButton("FINALIZAR");
	JButton Bsumar=new JButton(""+"+"+"");
	JButton Brest=new JButton(""+"-"+"");
	JButton Bmult=new JButton(""+"*"+"");
	JButton Bdiv=new JButton(""+"/"+"");
	JButton Bigual=new JButton(""+"="+"");
	JButton Bborrar=new JButton(""+"C"+"");

	JButton botonC[]=new JButton[10];
	JTextField textfield1,textfield2;
	
	String mem1;
	String mem2;
	String signo;
	
	
	
	public Calculadora()
	{
		int i,x,y;
		setLayout(null);
		//System.out.println("FOR PARA EL 0");
		for(i=0;i<1;i++)
		{
			botonC[i]=new JButton(""+i+"");
			x=(0+i*100)+200;
			y=(0+i*10)+10;
			
			//System.out.println("valor x= "+x+" valor y= "+y);
			botonC[i].setBounds(x,200,70,40);
			add(botonC[i]);
			botonC[i].addActionListener(this);
		}
		//System.out.println("PRIMER FOR");
		for(i=1;i<4;i++)
		{
			botonC[i]=new JButton(""+i+"");
			x=0+i*100;
			y=0+i*10;
			
			//System.out.println("valor x= "+x+" valor y= "+y);
			
			botonC[i].setBounds(x,50,70,40);
			add(botonC[i]);
			botonC[i].addActionListener(this);
		}
		//System.out.println("SEGUNDO FOR");
		for(i=4;i<7;i++)
		{
			botonC[i]=new JButton(""+i+"");
			x=(0+i*100)-300;
			y=0+i*10;
			
			//System.out.println("valor x= "+x+" valor y= "+y);
			botonC[i].setBounds(x,100,70,40);
			add(botonC[i]);
			botonC[i].addActionListener(this);
		}
		//System.out.println("TERCER FOR");
		for(i=7;i<10;i++)
		{
			botonC[i]=new JButton(""+i+"");
			x=(0+i*100)-600;
			y=0+i*10;
			//System.out.println("valor x= "+x+" valor y= "+y);
			botonC[i].setBounds(x,150,70,40);
			add(botonC[i]);
			botonC[i].addActionListener(this);
		}
		textfield1=new JTextField();
		textfield1.setBounds(10,10,300,30);
		add(textfield1);
		
		/*textfield2=new JTextField();
		textfield2.setBounds(10,50,100,30);
		add(textfield2);*/
		
		Bsumar.setBounds(0,50,60,40);
		add(Bsumar);
		Bsumar.addActionListener(this);
		
		Brest.setBounds(0,100,60,40);
		add(Brest);
		Brest.addActionListener(this);
		
		Bmult.setBounds(0,150,60,40);
		add(Bmult);
		Bmult.addActionListener(this);
		
		Bdiv.setBounds(0,200,60,40);
		add(Bdiv);
		Bdiv.addActionListener(this);
		
		Bigual.setBounds(300,200,70,40);
		add(Bigual);
		Bigual.addActionListener(this);
		
		Bborrar.setBounds(100,200,70,40);
		add(Bborrar);
		Bborrar.addActionListener(this);
		
		boton1.setBounds(300,300,100,40);
		add(boton1);
		boton1.addActionListener(this);

	}
	public void actionPerformed(ActionEvent e)
	{

		
		if(e.getSource()==botonC[0])
		{
			textfield1.setText(textfield1.getText()+"0");
		}
		if(e.getSource()==botonC[1])
		{
			textfield1.setText(textfield1.getText()+"1");
		}
		if(e.getSource()==botonC[2])
		{
			textfield1.setText(textfield1.getText()+"2");
		}
		if(e.getSource()==botonC[3])
		{
			textfield1.setText(textfield1.getText()+"3");
		}
		if(e.getSource()==botonC[4])
		{
			textfield1.setText(textfield1.getText()+"4");
		}
		if(e.getSource()==botonC[5])
		{
			textfield1.setText(textfield1.getText()+"5");
		}
		if(e.getSource()==botonC[6])
		{
			textfield1.setText(textfield1.getText()+"6");
		}
		if(e.getSource()==botonC[7])
		{
			textfield1.setText(textfield1.getText()+"7");
		}
		if(e.getSource()==botonC[8])
		{
			textfield1.setText(textfield1.getText()+"8");
		}
		if(e.getSource()==botonC[9])
		{
			textfield1.setText(textfield1.getText()+"9");
		}
		
		
		if(e.getSource()==Bborrar)
		{
			String cadena;
			cadena=textfield1.getText();
			if(cadena.length()>0)
			{
				cadena=cadena.substring(0,cadena.length()-1);
				textfield1.setText(cadena);
			}
		}
		
		if(e.getSource()==boton1)
		{
			System.exit(0);
		}
		if(e.getSource()==Bsumar)
		{
			if(!textfield1.getText().equals(""))
			{
				mem1=textfield1.getText();
				signo="+";
				textfield1.setText("");
			}
		}
		if(e.getSource()==Brest)
		{
			if(!textfield1.getText().equals(""))
			{
				mem1=textfield1.getText();
				signo="-";
				textfield1.setText("");
			}
		}
		if(e.getSource()==Bmult)
		{
			if(!textfield1.getText().equals(""))
			{
				mem1=textfield1.getText();
				signo="*";
				textfield1.setText("");
			}
		}
		if(e.getSource()==Bdiv)
		{
			if(!textfield1.getText().equals(""))
			{
				mem1=textfield1.getText();
				signo="/";
				textfield1.setText("");
			}
		}
		if(e.getSource()==Bigual)
		{
			int resultado=0;
			String resp;
			mem2=textfield1.getText();
			int val1=Integer.parseInt(mem1);
			int val2=Integer.parseInt(mem2);
			if(!mem2.equals(""))
			{
				if(signo.equals("+"))
				{
					resultado=val1+val2;
				}
					if(signo.equals("-"))
				{
					resultado=val1-val2;
				}
				if(signo.equals("*"))
				{
					resultado=val1*val2;
				}
				if(signo.equals("/"))
				{
					resultado=val1/val2;
				}
				resp=String.valueOf(resultado);
				textfield1.setText(resp);
			}
		}
	}

	
	public static void main (String [] ar)
	{
		Calculadora boton1=new Calculadora();
		boton1.setBounds(0,0,500,450);
		boton1.setVisible(true);
	}
}