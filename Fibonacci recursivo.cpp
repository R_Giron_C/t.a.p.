#include <stdio.h>
#include <stdlib.h>
#include <iostream>

using namespace std;

long long int fibonacci(int n)
{
	if(n==0||n==1)
	{
		return n;
	}
	else
	{
		return fibonacci(n-1)+fibonacci(n-2);
	}
}

int main ()
{
	int num;
	cout<<"Ingrese la cantidad de numeros de la serie: ";
	cin>>num;
	
	cout<<"Serie Fibonacci: "<<endl;
	for(int i=0; i<num ;i++)
	{
		cout<<fibonacci(i)<<endl;
	}
	
	return 0;
}
